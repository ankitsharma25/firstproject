<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


use App\Post;
use App\User;
use App\Role;
use App\Country;
use App\Photo;
use App\Address;
use App\Staff;
use App\Product;
use App\Tag;
use App\Video;
use Carbon\Carbon;


Route::get('/', function () {
    //return redirect(route("foo.route"));
});


//naming a url
Route::get('/foo', function () {
    return view('foo');
})->name('foo.route');

//getting parameters
Route::get('/bar/{name}', function ($name) {
    return $name;
});


//naming url
Route::get('admin/posts',array('as'=>'admin.home',function (){
    $url = route("admin.home");
    return "this url is ".$url;
}));

//first controller route
Route::get('/post/{id}','PostController@show_post')->where('id','[0-9]+');


Route::resource('posts','PostController');

//Calling a method
Route::get('/contact','PostController@contact');





/*
 * Database CRUD
 * Using RAW sql queries
 */
//Insert
Route::get('/insert',function(){
    DB::insert("insert into posts (title,content,user_id,post_id) VALUES (?,?,?,?)",['some title 32','Some content 23',23,23]);
});

//Read
Route::get('/read',function (){
   $results = DB::select("select * from posts where id = ?",[1]);
   return $results;
});

//Update
Route::get('/update',function(){
    $updated = DB::update("update posts set title = 'Updated title' where id = ?",[1]);
    return $updated;
});

//Delete
Route::get('/delete',function (){
    $deleted = DB::delete("delete from posts where id = ?" , [1]);
    return $deleted;
});


//-----------------------------Eloquent------------------------------------------//

/*
 * Eloquent
 * Crud operations using ORM
 */

//Get all data
Route::get('/readtable',function (){
    $posts = Post::all();
    foreach ($posts as $post){
        echo $post->title.'<br/>';
    }
});

//get data by id
Route::get('/find',function (){
    $posts = Post::find(3);
    return $posts->title;
});

//where clause
Route::get('/findwhere',function(){
    $posts = Post::where('id',3)->orderBy('id','desc')->take(1)->get();
    return $posts;
});

Route::get('/findmore',function (){
    $posts = Post::findOrFail(2);
    return $posts;
});

//Insert a new record
Route::get('/basicinsert',function (){
    $post = new Post;
    $post->title = 'New title';
    $post->content = 'New Content';
    $post->user_id = 4;
    $post->post_id = 4;
    $post->save();
});

/*
//Updating an existing data
Route::get('/basicinsert',function (){
    $post = Post::find(2);
    $post->title = 'New title 2';
    $post->content = 'New Content 2';
    $post->user_id = 4;
    $post->post_id = 4;
    $post->save();
});
*/

//
//Insert using maths assign
//We will use our model Post to assign the Title and content
//
Route::get('/create',function (){
    $post = Post::create([
        'Title'=>'hey its a new title 3',
        'content'=>'these are new contents'
    ]);
});

//update data using elequont
Route::get('/updatepost',function (){
    Post::where('id',2)->update([
        'title'=>'New php title h',
        'content'=>'New php content',
    ]);
});


//deleting records
Route::get('/deletepost',function (){
    $post = Post::find(3);
    $post->delete();

});
//deleting records using destroy and deleting multiple records
Route::get('/deletepost2',function (){
    Post::destroy(2);
    //Post::destroy(2,4); #We can use multiple ids to delete
});


//soft deleting or thrashing
Route::get('/softdelete',function (){
    Post::find(5)->delete();
    //Post::destroy(6);
});


//soft deleting or thrashing
Route::get('/readsoftdelete',function (){
    //$post = Post::find(9);
    //return selected item from thrash
    //$post = Post::withTrashed()->where('id',9)->get();

    //return all the items in thrash
    $post = Post::onlyTrashed()->where('is_admin',0)->get();
    return $post;
    //Post::destroy(6);
});

Route::get('/restore',function (){
    Post::withTrashed()->where('is_admin',0)->restore();
});

Route::get('/forcedelete',function (){
    Post::withTrashed()->where('id',2)->forceDelete();
});

//-------------------------------------//
//Relationships
// for joins and multiple relationships


//One to one relationship
//go to User model with the method post we have created a one to one relationship
//It will automatically detect the Post model via the namespace provided is hasOne function
Route::get('/user/{id}/post',function ($id){
    //return User::find($id)->post;
    return User::find($id)->post->title;
});


//Inverse of one to one relationship
//Calling the post model which eventually call User Model
Route::get('/post/{id}/user',function ($id){
    return Post::find($id)->user->name;
});


//----One to manny Relationships--//
//get all the post with user id = 1
Route::get('/getposts',function(){
    $user = User::find(1);
    foreach ($user->posts as $post){
        echo $post->title.'<br>';
    }
});

// Many to many relations
Route::get('getmany/{id}',function ($id){
   //Calling by method
    $user = User::find($id)->roles()->orderBy('id','desc')->get();
    return $user;
});


//Accessing intermediate table or pivot table
Route::get('user/pivot',function (){
    $user = User::find(1);
    foreach ($user->roles as $role){
        return $role->pivot->created_at;
    }
});

//Many through relation
//Find the post related to the country

Route::get('/user/country/{id}',function ($id){
    $country = Country::find($id);
    foreach ($country->posts as $post){
        return $post->title;
    }
});

//-------------Polymorphic Relations------------//

//We will use the namespace of the model in the table

Route::get('userphotos',function (){
    $user = User::find(1);
    foreach ($user->photos as $photo){
        return $photo->path;
    }
});


Route::get('post/photos',function (){
    $post = Post::find(2);
    foreach ($post->photos as $photo){
        return $photo->path;
    }
});

Route::get('photo/{id}/post',function ($id){
    $photo = Photo::findOrFail($id);
    return $photo->imageable_id;
});

//Many to many polymorphic relation
Route::get('post/tag',function (){
    $post = Post::find(2);
    foreach ($post->tags as $tag){
        echo $tag->name;
    }
});




//----------------------------------------------- CRUD -------------------------------------//

//---------------One to one Relationship----------------//



//Doing crud operations using one to one relationship like User one to one Address
//Using User table to do operations on Address tables

//----------Create---------//

//It will first check if the user exists
//We will create an object of address model
//We will pass data in the form of associative array to the Address instance
//Then we  will use user object to chain with the address method which lies in User model
//Then we call the save method
Route::get('/crud/insert',function(){
    $user = User::findOrFail(2);
    $address = new Address(['name'=>'1234 address hn 32 ']);
    $user->address()->save($address);
});

//---------Update---------//

//Create a user object with Address model
//Get address where User_Id is 1 and get the first record
//We will call $address->name and put the updated address in the name
//$address->save() this method will save the record

Route::get('crud/update',function(){
    //$address = Address::where('user_id',1);
    $address = Address::whereUserId(1)->first();
    $address->name = "Updated new address";
    $address->save();
});

//--------Read-----------//

Route::get('/crud/read',function(){
    $user = User::findOrFail(2);
    echo $user->address->name;
});

//-------Delete---------//
//Find the user with the id
//Call the delete method in the address

Route::get('/crud/delete',function (){
    $user = User::findOrFail(2);
    $user->address()->delete();
});



//---------------One to many-------------//

//---- Create ----//

Route::get('crud/onetomany/create',function(){
    $user = User::findOrFail(2);
    $post = new Post(['title'=>'Crud Title','content'=>'CRUD content']) ;
    $user->posts()->save($post);
 });


//----- Read -----//

Route::get('crud/onetomany/read',function (){
    $user = User::findOrFail(2);
    foreach ($user->posts as $post){
        echo $post->title.'<br/>';
    }

});


//----- Update ------//
Route::get('/crud/onetomany/update',function(){
    $user = User::find(1);
    $user->posts()->where('Id',1)->update(['title'=>'This is been updated by Crud','content'=>'Contents are updated via CRUD']);
});


//------ Delete ------//
Route::get('/crud/onetomany/delete',function(){
    $user = User::find(1);
    $user->posts()->whereId(1)->delete();
});


//--------------------- Many to Many -------------------//

//-------Create-------//

Route::get('/crud/manytomany/create',function (){
    $user = User::find(2);
    $role = new Role(['name'=>'Administrator']);
    $user->roles()->save($role);
});

//-------- Read --------//

Route::get('/crud/manytomany/read',function (){
    $user = User::findOrFail(2);
    foreach ($user->roles as $role){
        echo $role->name.'<br/>';
    }
});

//----------- Update -----------//
Route::get('/crud/manytomany/update',function (){
    $user = User::findOrFail(2);
    if($user->has('roles')){
        foreach ($user->roles as $role){
            if($role->name == 'Administrator'){
                $role->name = 'subs';
                $role->save();
            }
        }
    }
});

//---------- Deleting ----------//
Route::get('/crud/manytomany/delete',function(){
    $user = User::findOrFail(2);
    $user->roles()->delete();
});



//--------- Attaching and Detaching ---------//
//It is used for attaching a new role to user even
//If role already exist


Route::get('/crud/attach',function(){
    $user = User::findOrFail(1);
    $user->roles()->attach(4);
});

Route::get('/crud/detach',function(){
    $user = User::findOrFail(1);
    $user->roles()->detach(2);
});

Route::get('/crud/sync',function(){
    $user = User::findOrFail(1);
    $user->roles()->sync([6.2]);
});




//---------------- Polymorphic Relation Crud --------------//


//Create function insert records in the database table

Route::get('/crud/poly/create',function (){
    $staff = Staff::find(1);
    $staff->photos()->create(['path'=>'example.jpg']);
});

Route::get('/crud/poly/read',function (){
    $staff = Staff::findOrFail(1);
    foreach ($staff->photos as $photo){
        return $photo->path;
    }
});

Route::get('/crud/poly/update',function(){
    $staff = Staff::findOrFail(1);
    $photo = $staff->photos()->where('Id',4)->first(); // Id of the photos
    $photo->path = 'Updated2.jpg';
    $photo->save();
});

Route::get('/crud/poly/delete',function (){
    $staff = Staff::find(1);
    $staff->photos()->delete();
});



//------------------- Polymorphic many to many --------------------//

Route::get('/crud/poly/manytomany/create',function (){
    $tag1 = Tag::find(1);
    $post = Post::create(['name'=>'My first post']);
    $post->tags()->save($tag1);
    $tag2 = Tag::find(2);
    $video = Video::create(['name'=>'Myvideo.mov']);
    $video->tags()->save($tag2);
});

Route::get('/crud/poly/manytomany/read',function (){
    $post = Post::findOrFail(12);
    foreach ($post->tags as $tag ){
        echo $tag;
    }
});

Route::get('/crud/poly/manytomany/update',function (){
    $post = Post::findOrFail(12);
    foreach ($post->tags as $tag ){
        echo $tag->where('Name','Php')->update(['name'=>'Updated PHP']);
    }
});

Route::get('/crud/poly/manytomany/delete',function (){
    $post = Post::find(12);
    foreach ($post->tags as $tag ){
        echo $tag->where('Id',2)->delete();
    }
});





//--------------------------- FORMS ---------------------------//
// CRUD Application

Route::resource('/forms/posts','PostController');

Route::group(['middleware'=>'web'],function(){
    Route::resource('/forms/posts','PostController');
});





//---------------- CARBON ---------------//

Route::get('date',function (){
    echo '<br />';
    echo Carbon::now()->addDays(10)->diffForHumans();
    echo '<br />';
    echo Carbon::now()->subMonths(5)->diffForHumans();
    echo '<br />';
    echo Carbon::now()->yesterday()->diffForHumans();
});

//------- Accessors -------//

Route::get('accessors/getname',function (){
    $user = User::find(1);
    echo $user->name;
});


//------- Mutators --------//
Route::get('mutators/setname',function (){
    $user = User::find(1);
    $user->name = 'william';
    $user->save();
});

Auth::routes();

Route::get('/home', 'HomeController@index');
