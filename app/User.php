<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //set relation to one to one
    //here App\Post is the namespace of the post table
    public function post(){
        return $this->hasOne('App\Post');
    }

    //----------One to Many-----------//
    //set relation to one to many
    //Here App\Post is namespace for the Post model
    //It will automatically fetch all the base model methods
    public  function posts(){
        return $this->hasMany('App\Post');
    }



    //-----------Many to Many-------------//
    //Here in the second parameter we have defined the name of the table
    //Table name is defined when we have a table name different from default
    //Third parameter is the primary key of the table
    //Fourth parameter is the role id
    public function  roles(){
        //For customer table names and columns
        //return $this->belongsToMany('App\Role','role_user','user_id','role_id');
        return $this->belongsToMany('App\Role')->withPivot('created_at');
    }

    public  function photos(){
        return $this->morphMany('App\Photo','imageable');
    }

//--------------------- CRUD ---------------------//

    public function address(){
        return $this->hasOne('App\Address');
    }

    //accessors conventions get---Attribute
    public function getNameAttribute($value){
        return ucfirst($value);
    }

    //Mutators manipulate and set data
    public function setNameAttribute($value){
        $this->attributes['name'] = strtoupper($value);
    }
}
