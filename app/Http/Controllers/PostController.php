<?php

namespace App\Http\Controllers;
use App\Http\Requests\CreatePostRequest;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //query scope
        //It is used when we have a long query
        //We put the query in a static method in the Model i.e Post
        //in this case
        $posts = Post::latest();


        return view('posts.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        //
        //return $request->get('title');
        //return $request->title;
        //Post::create($request->all());

//        $input = $request->all();
//        $input['title'] = $request->title;
//        Post::create($request->all());

//        //validations
//        $this->validate($request,[
//            'title'=>'required'
//        ]);

//        $post = new Post; //Post in the object from the Post model
//        $post->title = $request->title; // We will put the title from Post model
//        $post->save(); // call save method to insert it into the database
//
//        return redirect('/posts');

         // Uploading files
        //   $file = $request->file('file'); #file name Random Genereated

        if($file = $request->file('file')){
            $name = $file->getClientOriginalName(); #original file name
            $file->move('images',$name);
            $input['path'] = $name;
        }
        Post::create($input);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post  = Post::findOrFail($id);
        return view('posts.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::findOrFail($id);
        return view('posts.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $post = Post::findOrfail($id);
        $post->update($request->all());
        return redirect('posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::findOrFail($id);
        $post->delete();
        return redirect('posts');
    }

    public function contact(){
        $people = ['Name1','Name2','Name3'];
        return view('contact',compact('people'));
    }

    Public function show_post($id,$name='ankit'){
        //return view('post')->with('id',$id);
        return view('post',compact('id','name'));
    }
}
