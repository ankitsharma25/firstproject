<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    //by default base modal is set Posts is considered as the default table
    //in the database

    // change table name from default
    //protected $table = 'posts';
    // change default primary key name from default id
    //protected  $primaryKey = 'post_id'

    protected $dates = ['deleted_at'];

    //We have assigned that the Title and content can be inserted
    //Maths assign
    protected $fillable = [
        'title',
        'content',
        'path'
    ];


    //Inverse of a one to one relationship
    //We wil fetch User form the post id
    public function user(){
        return $this->belongsTo('App\User');
    }

    //polymorphic relation many
    public function photos(){
        return $this->morphMany('App\photo','imageable');
    }

    public function tags(){
        return $this->morphToMany('App\Tag','taggable');
    }

    //query scope method
    public static function scopeLatest($query){
        return $query->orderBy('id','asc')->get();
    }

    //accessor
    public function getPathAttribute($value){
        return '/images/' . $value;
    }

}
