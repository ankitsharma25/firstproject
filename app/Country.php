<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //country id is defined for User i.e App\User
    //it will look for country id in users
    Public function posts(){
        //return $this->hasManyThrough('App\Post','App\User','country_id','the_user_id');
        return $this->hasManyThrough('App\Post','App\User');
    }
}
