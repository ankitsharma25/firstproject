@extends('layouts.app');
<h2>Edit Page</h2>


@section('content');
{!! Form::model($post,['action'=>['PostController@update',$post->id],'method'=>'PATCH']) !!}
    {!! Form::label('title','Title') !!}
    {{ csrf_field() }}
    {!! Form::text('title', null,['class'=>'form-control']) !!}
    {!! Form::submit('Update Post',['class'=>'btn btn-default']) !!}
{!! Form::close() !!}

{!! Form::model($post,['action'=>['PostController@destroy',$post->id],'method'=>'DELETE']) !!}
    {{csrf_field()}}
    {{Form::submit('Delete',['class'=>'btn btn-primary'])}}
{!! Form::close() !!}

@stop

@section('footer')
    {{--<script>alert('hello');</script>--}}
@stop