@extends('layouts.app')
<h2>Create Page</h2>


@section('content')
    {!! Form::open(['method'=>'post','action'=>'PostController@store','files'=>'true']) !!}
        {!! Form::label('title','Title') !!}
        {{ csrf_field() }}
        {!! Form::text('title', null,['class'=>'form-control']) !!}

        {!! Form::file('file', null,['class'=>'form-control']) !!}
        {!! Form::submit('Create Post',['class'=>'btn btn-default']) !!}
    {!! Form::close() !!}

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
@stop

@section('footer')
    {{--<script>alert('hello');</script>--}}
@stop