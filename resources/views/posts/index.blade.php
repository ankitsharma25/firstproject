@extends('layouts.app');
<h2>Index Page</h2>


@section('content');
<ul>
    @foreach($posts as $post)
        <li style="color:#000;"><a href="{{route('posts.show',$post)}}" >{{$post->title}}</a></li>
        <li><img height="100" src="{{$post->path}}"></li>
    @endforeach
</ul>
@stop

@section('footer')
    {{--<script>alert('hello');</script>--}}
@stop