@extends('layouts.app');
<h2>Contact Page</h2>
@section('content');
    @if(count($people))
        <ul>
            @foreach($people as $person)
                <li>{{$person}}</li>
            @endforeach
        </ul>
    @endif
@stop

@section('footer')
{{--<script>alert('hello');</script>--}}
    @stop